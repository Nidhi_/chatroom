# Chatroom

![Demo](demo/demo.gif)

## How to setup project?

To setup the project, follow these steps:
- Download or Clone the Project.
- Go to  [https://pusher.com/](https://pusher.com/) and create an account.
-- Create a Channel of name "Chatroom".
-- Go to Chatroom/debug-console to see the response of events.
-  Create an .env file and replace it's credentials with yours. Place your Pusher's credentials in .env file.
-  Now run these commands in your project directory:
    ```sh
        $ composer  install
        $ npm install
        $ php artisan migrate
    ```
- To locally run the project in browser, run the following commands:
    ```sh
        $ php artisan serve
        $ npm run watch
    ```

## How it will work?

Follow these steps:
- Register 2-3 accounts in project.
- Open each accounts in different browsers so that you can see how the chat can be done in between different users.
- Message from each users chat.
- You can see how the users receives messages and you will also get notifications if any other user joins or left the chat.
- You can also delete the whole chat.